package systems.comodal.tzlibre;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

final class TezosBase58 {

  private TezosBase58() {
  }

  private static final char[] ALPHABET = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz".toCharArray();
  private static final char ENCODED_ZERO = ALPHABET[0];


  private static final byte[] TEZOS_ADDRESS_VERSION = new byte[]{0x06, (byte) 0xa1, (byte) 0x9f};

  static String toBase58(final byte[] pubKeyHash) throws NoSuchAlgorithmException {
    final int prefixAndHash = TEZOS_ADDRESS_VERSION.length + pubKeyHash.length;
    final byte[] out = new byte[prefixAndHash + 4];
    System.arraycopy(TEZOS_ADDRESS_VERSION, 0, out, 0, TEZOS_ADDRESS_VERSION.length);
    System.arraycopy(pubKeyHash, 0, out, TEZOS_ADDRESS_VERSION.length, pubKeyHash.length);
    final var sha256 = MessageDigest.getInstance("SHA-256");
    sha256.update(out, 0, prefixAndHash);
    final byte[] checksum = sha256.digest(sha256.digest());
    System.arraycopy(checksum, 0, out, prefixAndHash, 4);
    return TezosBase58.encode(out);
  }

  private static String encode(byte[] input) {
    if (input.length == 0) {
      return "";
    }
    // Count leading zeros.
    int zeros = 0;
    while (zeros < input.length && input[zeros] == 0) {
      ++zeros;
    }
    // Convert base-256 digits to base-58 digits (plus conversion to ASCII characters)
    input = Arrays.copyOf(input, input.length); // since we modify it in-place
    final char[] encoded = new char[input.length * 2]; // upper bound
    int outputStart = encoded.length;
    for (int inputStart = zeros; inputStart < input.length; ) {
      encoded[--outputStart] = ALPHABET[divMod(input, inputStart, 256, 58)];
      if (input[inputStart] == 0) {
        ++inputStart;
      }
    }
    // Preserve exactly as many leading encoded zeros in output as there were leading zeros in
    // input.
    while (outputStart < encoded.length && encoded[outputStart] == ENCODED_ZERO) {
      ++outputStart;
    }
    while (--zeros >= 0) {
      encoded[--outputStart] = ENCODED_ZERO;
    }
    // Return encoded string (including encoded leading zeros).
    return new String(encoded, outputStart, encoded.length - outputStart);
  }

  /**
   * Divides a number, represented as an array of bytes each containing a single digit
   * in the specified base, by the given divisor. The given number is modified in-place
   * to contain the quotient, and the return value is the remainder.
   *
   * @param number     the number to divide
   * @param firstDigit the index within the array of the first non-zero digit (this is
   *                   used for optimization by skipping the leading zeros)
   * @param base       the base in which the number's digits are represented (up to 256)
   * @param divisor    the number to divide by (up to 256)
   * @return the remainder of the division operation
   */
  private static byte divMod(final byte[] number, final int firstDigit, final int base, final int divisor) {
    // this is just long division which accounts for the base of the input digits
    int remainder = 0;
    for (int i = firstDigit; i < number.length; i++) {
      final int num = remainder * base + ((int) number[i] & 0xFF);
      number[i] = (byte) (num / divisor);
      remainder = num % divisor;
    }
    return (byte) remainder;
  }
}
