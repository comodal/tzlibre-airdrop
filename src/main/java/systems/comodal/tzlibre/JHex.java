package systems.comodal.tzlibre;

import java.util.Arrays;

final class JHex {

  private JHex() {
  }

  private static final char[] LOWER_HEX = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
      'a', 'b', 'c', 'd', 'e', 'f'};
  private static final char[] UPPER = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
      'A', 'B', 'C', 'D', 'E', 'F'};
  private static final int MAX_CHAR = 'f';
  private static final int[] DIGITS = new int[MAX_CHAR + 1];

  static {
    Arrays.fill(DIGITS, -1);
    for (final char c : LOWER_HEX) {
      DIGITS[c] = Character.digit(c, 16);
    }
    for (int i = 10; i < UPPER.length; ++i) {
      final char c = UPPER[i];
      DIGITS[c] = Character.digit(c, 16);
    }
  }

  static String encode(final byte[] data) {
    final int len = data.length;
    final char[] hex = new char[len << 1];
    for (int i = 0, h = 0, d; i < len; ) {
      d = data[i++] & 0xff;
      hex[h++] = LOWER_HEX[d >>> 4];
      hex[h++] = LOWER_HEX[d & 0xf];
    }
    return new String(hex);
  }

  static byte[] decode(final CharSequence chars) {
    final byte[] data = new byte[chars.length() >> 1];
    if (data.length == 0) {
      return data;
    }
    for (int i = 0, c = 0; ; ++c) {
      data[i++] = (byte) (DIGITS[chars.charAt(c)] << 4 | DIGITS[chars.charAt(++c)]);
      if (i == data.length) {
        return data;
      }
    }
  }
}
