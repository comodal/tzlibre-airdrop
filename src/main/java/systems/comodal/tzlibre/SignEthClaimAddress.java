package systems.comodal.tzlibre;

import net.i2p.crypto.eddsa.EdDSAEngine;
import net.i2p.crypto.eddsa.EdDSAPrivateKey;
import net.i2p.crypto.eddsa.EdDSAPublicKey;
import net.i2p.crypto.eddsa.spec.EdDSAPrivateKeySpec;
import net.i2p.crypto.eddsa.spec.EdDSAPublicKeySpec;
import org.bitcoinj.crypto.PBKDF2SHA512;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.*;
import java.text.Normalizer;
import java.util.Arrays;

import static java.nio.charset.StandardCharsets.UTF_8;
import static net.i2p.crypto.eddsa.spec.EdDSANamedCurveTable.ED_25519_CURVE_SPEC;
import static systems.comodal.tzlibre.TezosBase58.toBase58;

public final class SignEthClaimAddress {

  public static void main(final String[] args) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
    Security.addProvider(new BouncyCastleProvider());

    final var email = args[0];
    final var mnemonic = args[1];
    final var password = args[2];
    final var ethAddress = args[3];

    final var salt = "mnemonic" + Normalizer.normalize(new String((email + password).getBytes(UTF_8), UTF_8), Normalizer.Form.NFKD);
    final var seed = PBKDF2SHA512.derive(mnemonic, salt, 2048, 64);
    final var keyPair = new EdDSAPrivateKeySpec(Arrays.copyOf(seed, 32), ED_25519_CURVE_SPEC);
    final var publicKey = keyPair.getA().toByteArray();

    final var blake2b512 = MessageDigest.getInstance("BLAKE2B-512");
    final var msgHash = blake2b512.digest(JHex.decode(ethAddress.replaceFirst("0x", "")));
    final var eddsaEngine = new EdDSAEngine(MessageDigest.getInstance(ED_25519_CURVE_SPEC.getHashAlgorithm()));
    eddsaEngine.initSign(new EdDSAPrivateKey(keyPair));
    final var signature = eddsaEngine.signOneShot(msgHash);
    final var blake2b160 = MessageDigest.getInstance("BLAKE2B-160");
    System.out.printf("%nTezos email: '%s'%n", email);
    System.out.printf("Tezos address: '%s'%n", toBase58(blake2b160.digest(publicKey)));
    System.out.printf("Ethereum address: '%s'%n", ethAddress);
    System.out.printf("Tezos Public Key: '%s'%n", JHex.encode(publicKey));
    System.out.printf("Tezos signature of ETH address: '%s'%n", JHex.encode(signature));

    eddsaEngine.initVerify(new EdDSAPublicKey(new EdDSAPublicKeySpec(publicKey, ED_25519_CURVE_SPEC)));
    System.out.println("Signature Valid: " + eddsaEngine.verifyOneShot(msgHash, signature));
  }
}
