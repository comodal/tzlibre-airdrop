Script to generate a TZL claim signature to be used at [tzlibre.github.io/sign](https://tzlibre.github.io/sign.html)

## Requirements
* [JDK 10](http://www.oracle.com/technetwork/java/javase/downloads/jdk10-downloads-4416644.html)

## How to run?
```bash
./gradlew --no-daemon --console=plain -q signClaimAddress
```

## What to expect?
```bash
./gradlew --no-daemon --console=plain -q signClaimAddress
WARNING: An illegal reflective access operation has occurred
WARNING: Illegal reflective access using Lookup on org.gradle.internal.reflect.JavaMethod (file:/Users/jamesedwards/.gradle/wrapper/dists/gradle-4.8-bin/divx0s2uj4thofgytb7gf9fsi/gradle-4.8/lib/gradle-base-services-4.8.jar) to class java.lang.ClassLoader
WARNING: Please consider reporting this to the maintainers of org.gradle.internal.reflect.JavaMethod
WARNING: Use --illegal-access=warn to enable warnings of further illegal reflective access operations
WARNING: All illegal access operations will be denied in a future release

Enter Tezos email: freetezos@gmail.com
Tezos Mnemonic: 
Tezos Password: 
Ethereum Claim Address: 0xETHADDRESS

Tezos email: 'freetezos@gmail.com'
Tezos address: 'tz1MRfu4dEPTg929ksnVR3XtKZx5xeokMqNh'
Ethereum address: '0xETHADDRESS'
Tezos Public Key: 'dda571123bf05e5776898f375bd24a1df5ac9717931d62c5449657771257c1ca'
Tezos signature of ETH address: 'e90511e7f8bdf8d6eebdac637e78bc1425aee1b697db05c06da6e739164ad42702344f9adf134740aa93093c48022c85f0398979494964b88ac65c00b7adc701'
Signature Valid: true
```

Note: Input is hidden from the console when inputting the Tezos Mnemonic and Password.

